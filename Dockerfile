FROM python:3

WORKDIR /app

RUN pip3 install lxml requests passlib imapclient pandas beautifulsoup4 https://github.com/paulscherrerinstitute/py_elog/archive/1.3.9.zip

RUN sed -i 's/MinProtocol = TLSv1.2/MinProtocol = TLSv1.0/' /etc/ssl/openssl.cnf
COPY runner.py runner.py
USER 10001

CMD ["python3", "runner.py"]