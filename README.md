# Elog email alarms
This is a repo containing the source and the Docker image
to run a script that is checking email on a CERN service
account, parse them and insert the parsed content into 
an elog container.

To build the docker image:
If you're using a CERN repo you can use the gitlab's container registry
and build an image like this:

```bash
docker build -t gitlab-registry.cern.ch/grigolet/elog-email-alarms:latest .
```

# Configuration and run
To run the image you should provide a path to a `configs.json` file.
The `configs.json` file contains the necessary values to run correctly
the script.

## Description of `configs.json`
An example of `configs.json` file looks like this:

```python
{
    "email_hostname": "imap.cern.ch", # the email server hostname
    "email_port": 993, # email server port
    "email_user": "user", # authentication user
    "email_password": "password",
    "elog_hostname": "http://localhost:8080/",  # the elog hostname. Notice the / at the end
    "elog_alarm_section": "Alarms/",  # the logbook on which to send the alarms parsed from the email
    "elog_alarm_stats_section": "Alarms_statistics/", # the logbook where the alarms counts are stored
    "elog_user": "user", # elog user to post entries
    "elog_password": "password",
    "elog_use_ssl": false, # for cern websites you cak skip ssl verification
    "elog_author": "Gas Dip (bot)", # the name that will displayed in 'Author' field when posting the entry
    "runner_time_delta": "5d", # The number of days, hours or week to go back in the past to retrieve emails
    "runner_mark_read_messages": false, # if true the emails will be marked as read once posted
    "runner_time_sleep": 600 # The time in seconds to sleep before checking emails and inserting them again
}
```
Additional to this there are two environmental variables that you can 
use to set the logging level and the path to the `configs.json` file:
```bash
LOGGING_LEVEL=10
CONFIG_PATH=/app/configs.json
```

## Run the docker image
An example command may look like this
```bash
docker run \
-v $(pwd)/configs.json:/app/configs.json \
-e "LOGGING_LEVEL=10" \
-e "CONFIG_PATH=/app/configs.json" \
--rm \
gitlab-registry.cern.ch/grigolet/elog-email-alarms:latest
```

# Structure
The wrapper script in the docker image is running two processes: the first is the elog
web server and the second is a python script running periodically to fetch emails, parse them and insert the result as elog entry. The python script uses [py_elog](https://github.com/paulscherrerinstitute/py_elog) to insert new entries 