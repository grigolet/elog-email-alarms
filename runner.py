import elog
from imapclient import IMAPClient, SEEN
import email
import ssl
import re
import quopri
import datetime
import pandas as pd
import json
import os
import logging
import urllib3
import requests
import sys
from bs4 import BeautifulSoup
import time
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


class ElogEmailHandler:
    def __init__(self, hostname, port, user, password):
        """Create a ssl context to skip ssl certification
        and create the handler object with hte proper credentials"""
        self.ssl_context = ssl.create_default_context()
        # don't check if certificate hostname doesn't match target hostname
        self.ssl_context.check_hostname = False
        # don't check if the certificate is trusted by a certificate authority
        self.ssl_context.verify_mode = ssl.CERT_NONE
        self.hostname = hostname
        self.port = port
        self.user = user
        self.password = password
        self.imap_client = IMAPClient(
            host=hostname, port=port, ssl_context=self.ssl_context)

    def get_emails(self, readonly=True, criteria=['UNSEEN', 'NEW']):
        """Return a list of dicts containing the emails corresponding to the
        desired criteria
        :param readonly bool: Open the client in readonly mode, no access to change flags
        :param criteria list: list of flags to use to retrieve the emails
        :return dict: A dictionary with the following keys:
            - id: email id
            - date: the date parsed from the server
            - subject: subject of the email
            - from: the email sender
            - text: the email text content
        """
        emails = []
        with IMAPClient(host=self.hostname, port=self.port, ssl_context=self.ssl_context) as client:
            client.login(self.user, self.password)
            client.select_folder('INBOX', readonly=readonly)
            messages = client.search(criteria)
            data = client.fetch(messages, 'RFC822')
            for uid, message in data.items():
                email_message = email.message_from_bytes(message[b'RFC822'])
                emails.append({
                    'id': uid,
                    'date': email.utils.parsedate_to_datetime(email_message['Date']),
                    'subject': email_message['Subject'],
                    'from': email_message['From'],
                    'text': email_message.get_payload()
                })

            return emails

    def parse_email(self, sender, subject, text):
        """Parse the main information from alarms email
        given the sender, the subject and the text.
        :param sender str: the email sender
        :param subject str: the email subject
        :param text str: the email content
        :return dict: return a dict with the following keys:
            - experiment: like 'ATLAS', 'CMS', 'NA62', etc.
            - system: like 'CMSRPC', 'ATLAUX', etc.
            - alarm_name: like 'CMSDT_Di_PCV6326RLimAl'
            - alarm_timestamp: timestamp parsed from the subject message
            - alarm_status: like 'HH', 'Ok', 'Bad (Ack)', etc.
            - value: the value of the crossed threshold
            - 'parsed_text': the text of the message, usually found
                after the value of the crossed threshold
            - 'category': the category key present in the content of the 
                email, usually 'unProcessAlarm'
        Experiment: ATLAS, ALICE, CMS, LHCb, NA62, CLOUD, etc.
        System: AUX, 
        """
        r_experiment = r'<(.+)(GCS|PGS)(.*)@cern.ch>'  # not reliable as some messages have no sender configured
        r_experiment_res = re.search(r_experiment, sender)
        # This could be an empty string because some emails have no sender!
        experiment = r_experiment_res.groups()[0].strip(
            '_-') if r_experiment_res else sender
        experiment = experiment.upper()
        # Legacy, waiting for UNICOS patch
        r_subject_1 = r'(?P<status>.+ )?(?P<system>.+?)_(?P<module_device>(?:(.+?)_)?(.+))- (?P<month>\d\d)/(?P<day>\d\d)/(?P<year>\d\d\d\d) +(?P<hours>\d\d):(?P<minutes>\d\d):(?P<seconds>\d\d)'
        # New one after UNICOS patch
        r_subject_2 = r'(?P<status>.+)? (?P<system>.+?)_(?P<module_device>(?:(.+?)_)?(.+))- (?P<year>\d\d\d\d)-(?P<month>\d\d)-(?P<day>\d\d) +(?P<hours>\d\d):(?P<minutes>\d\d):(?P<seconds>\d\d).(?P<millis>\d\d\d)'
        r_category = r'((\w+)[_?])?(.+)'
        groups = re.search(r_subject_1, subject)
        if not groups:
            groups = re.search(r_subject_2, subject)
        if not groups:
            logging.error(f"Regex for subject did not work. Regex: {r_subject_2} Subject:{subject}")
        regex_values = groups.groupdict()
        alarm_status = regex_values.get('status')
        system = regex_values.get('system')
        module_device = regex_values.get('module_device')
        year = regex_values.get('year')
        month = regex_values.get('month')
        day = regex_values.get('day')
        hours = regex_values.get('hours')
        minutes = regex_values.get('minutes')
        seconds = regex_values.get('seconds')
        alarm_timestamp = datetime.datetime(int(year), int(
            month), int(day), int(hours), int(minutes), int(seconds))
        alarm_name = f'{system}_{module_device}'
        lines = text.splitlines()
        r_value = r'Value=([-+]*\d+.?\d*) (.*)'
        value = ''
        category = ''
        parsed_text = ''
        for line in lines:
            if 'Value=' in line:
                logging.debug('Searching for Value in line %s', line)
                value, parsed_text = re.search(r_value, line).groups()
                value = float(value)
            if 'Category:' in line:
                category_cleaned = line.replace('Category:', '').strip()
                _, _, category = re.search(
                    r_category, category_cleaned).groups()

        return {
            'experiment': experiment,
            'system': system,
            'alarm_name': alarm_name,
            'alarm_timestamp': alarm_timestamp,
            'alarm_status': alarm_status,
            'value': value,
            'parsed_text': parsed_text,
            'category': category
        }

    def mark_email(self, email_ids, flags=[SEEN]):
        """Mark the specified email id with the desired flag
        :param email_ids list: list of ids of emails
        :param flags list: list of flags to use to mark emails"""
        statuses = []
        # Always create a new IMAPclient object as the context manager
        # of IMAP client sends a LOGOUT command from which it's
        # not possible to login again (IMAP implementation)
        with IMAPClient(host=self.hostname, port=self.port, ssl_context=self.ssl_context) as client:
            client.login(self.user, self.password)
            client.select_folder('INBOX', readonly=False)
            statuses.append(client.add_flags(email_ids, flags))

        return statuses


class ElogAlarmsStats:
    """This class provides the utilities to count the
    alarms, group them, categorize them by rowking/non working
    hours, etc."""

    def __init__(self, emails, url_calendar='https://home.cern/official-holidays'):
        """ Initialize the objects with the emails, the url to
        the calendar and a pandas Series of timestamps indicating
        the CERN festivities
        :param emails pd.DataFrame: contains all the emails with the parsed info.
            The required columns are:
            - date: the date of the email received
            - experiment: like CMS, NA62, etc.
            - subject: the subject of the email
            - category: usually PGS
        :param url_calendar: the url to the cern official holidays.
            The url is used to parse information on the official 
            festivites at CERN

        """
        self.emails = emails
        self.url_calendar = url_calendar
        self.exclude_list = self.parse_calendar(self.url_calendar)

    def get_df_stats(self):
        """Compute a dataframe of different rows, depending on the number
        of the weeks and the number of the experiment the email are referring
        to.
        :return pd.DataFrame: The columns of the returned dataframe are:
            - experiment
            - week
            - Alarms during working hours
            - Alarms outside working hours
            - Warning during working hours
            - Warning outside working hours
            The tuple (experiment, week) is unique in the dataframe
        """
        df = self.emails
        # First, get the week from the date
        df['week'] = df['date'].dt.isocalendar().week
        df['year'] = df['date'].dt.isocalendar().year
        # Get the outside_working_hours flag
        df['outside_working_hours'] = df.date.apply(
            lambda x: self.is_outside_working_hour(x, self.exclude_list))
        df['time_window'] = (df.outside_working_hours
                             .map({True: 'outside working hours', False: 'during working hours'})
                             .astype('category'))
        # Create a column indicating if the category is alarm or warning by checking
        # the text inside the column. This is done because the category can be
        # unProcessAlarm, PGSAlarmSupply13, etc.
        df['category_parsed'] = (df.category
                                 .apply(lambda x: 'Alarms' if 'alarm' in x.lower() else 'Warnings')
                                 .astype('category')
                                 .cat.set_categories(['Alarms', 'Warnings']))
        # Create a dataframe with only the relevant column in case it contains more
        df2 = df[['experiment', 'year', 'week',
                  'category_parsed', 'time_window', 'subject']]
        # Group by the columns that will be the unique keys in the elog entry (i.e. the columns of the elog tab)
        grouped = df2.groupby(
            ['experiment', 'year', 'week', 'category_parsed', 'time_window'])
        # now we want to extract two information: 1. the number of entries for each group
        # and second a concatenated string of subjects to be put as elog text of the entry
        aggregated = grouped.subject.agg(['count', lambda x: '\n'.join(x)])
        # Rename the columns and reset the index
        dfres = aggregated.rename(
            columns={'<lambda_0>': 'subjects'}).reset_index()
        # We want to pivot the columns so that we have a dataframe looking like this
        #                 | alarm                    | warning                    |
        # |experiment|week| during w.h.| outside w.h.| during w.h. | outside w.h. |
        #     CMS    | 31 | 1          | 0           | 12          | 1            |
        # ...
        pivoted = dfres.pivot(index=['experiment', 'year', 'week'], columns=[
                              'category_parsed', 'time_window'], values=['count', 'subjects'])
        countdf = pivoted['count']
        subjectsdf = pivoted['subjects']
        # Flatten the columns names: alarm + during .w.h -> alarm during w.h.
        countdf.columns = countdf.columns.map(' '.join)
        # Drop lines where all the values all NaNs (meaning that are only generated by the pivot but no alarms are present)
        countdf = countdf.dropna(how='all')
        # convert all NaN to int and set them to 0. Convert all the dataframe to integers
        countdf = countdf.fillna(0).astype(int)
        # Append the subjects columns with all the strings
        # Needs some manipulation
        subjects = subjectsdf.dropna(how='all')
        subjects = subjects.fillna('')
        grouped_subjects = subjects.groupby(level=[0, 1, 2])
        # This is to transform a df looking like this:
        # experiment  week
        # ATLAS       34      LL ATLAUX_Pr_18ATankLevel- 08/20/2020  13:51:5...
        # CMS         33      HH CMSDT_Di_PCV6326RLimAl- 08/16/2020  20:45:1...
        #             34      HH CMSRPC_Di_PCV6928RLimAl- 08/17/2020  12:21:...
        # NA62        34      L Straw_Di_PT6375- 08/17/2020  11:16:45 (milli...
        #
        #

        grouped_subjects = grouped_subjects.apply(
            lambda x: ''.join(x.values.flatten()))
        countdf['subjects'] = grouped_subjects
        countdf = countdf.reset_index()

        return countdf

    def is_outside_working_hour(self, timestamp, exclude_list, start='08:30', stop='17:30'):
        """Return a boolean indicating if 'timestamp' is outside the CERN
        working hours.
        :param timestamp pd.Timestamp: the timestamp to check
        :param exclude_list pd.Series: a series of dates to exclude from working days
        :param start str: the start of the working hour
        :param stop str: the stop of the working hour
        :return bool: if timestamp is within a working hour"""
        # First, check if the date of the timestamp is not in the exclude_list
        is_in_exclude_list = timestamp.date() in set(exclude_list.dt.date)
        # Second, check if the day is weekday or weekend
        is_weekend = timestamp.weekday() > 4
        # Third, check if the time falls in the specified range
        hour_start, minutes_start = re.search(r'(\d+):(\d+)', start).groups()
        minutes_start = int(hour_start) * 60 + int(minutes_start)
        hour_stop, minutes_stop = re.search(r'(\d+):(\d+)', stop).groups()
        minutes_stop = int(hour_stop) * 60 + int(minutes_stop)
        is_before_start = timestamp.hour * 60 + timestamp.minute < minutes_start
        is_after_stop = timestamp.hour * 60 + timestamp.minute > minutes_stop

        return is_in_exclude_list | is_weekend | is_before_start | is_after_stop

    def parse_calendar(self, url='https://home.cern/official-holidays'):
        """Tries to parse the festivities from the official
        CERN url stating the holidays of the current year
        :param calendar str: the url to the cern official holidays.
            By default it's https://home.cern/official-holidays"""
        # Read the year from the inside of the web page
        req = requests.get(url)
        res = req.content
        soup = BeautifulSoup(res, 'html.parser')
        text = soup.text
        regex = r'Official holidays in (\d+) at a glance'
        groups = re.search(regex, text).groups()
        year = groups[0]
        # Read the calendar
        calendar = pd.read_html(url)[0]
        # Rename the columns
        calendar.columns = ['day', 'date', 'description']
        # Add the year to the date column
        calendar['date'] = calendar.date + ' ' + year
        # Parse the date into a pandas Timestamp
        calendar['parsed_date'] = pd.to_datetime(
            calendar.date, format='%d %B %Y')

        # Now go to the specific year url
        req = requests.get(f'{url}/{year}')
        res = req.content
        soup = BeautifulSoup(res, 'html.parser')
        text = soup.text.replace('\xa0', ' ')
        regex_yearly_closure = r'The Laboratory will be closed from (.+) (\d+ \D+ \d+) to (.+) (\d+ \D+ \d+) inclusive'
        result_regex = re.search(regex_yearly_closure, text)
        if result_regex:
            groups = result_regex.groups()
            day1, date_from, day2, date_to = groups
            # Create a list of dates using the 'from' and 'to' parsed
            # from the regex. The list will be appended to the dates
            # parsed from the table
            date_from = pd.to_datetime(date_from, format='%d %B %Y')
            date_to = pd.to_datetime(date_to, format='%d %B %Y')
            dates_yearly_closure = pd.Series(pd.date_range(date_from, date_to))
            dates = pd.concat([calendar.parsed_date, dates_yearly_closure])
        else:
            dates = calendar.parsed_date

        return dates


def main():
    # Setup the logger with the time, logging level, line number and message
    # The logging level can be also changed using the environmental variable
    # 'LOGGING_LEVEL', otherwise it will default to logging.DEBUG (=10)
    logger = logging.getLogger(__name__)
    logging_level = int(os.environ.get('LOGGING_LEVEL', logging.DEBUG))
    logging.basicConfig(
        level=logging_level, format='%(asctime)s - %(name)s - %(levelname)s - L%(lineno)s - %(message)s')
    # Try to open the json configs to run the program. If no environmental variable
    # is define it will try to open the 'configs.json' file
    config_path = os.environ.get('CONFIG_PATH', '/app/configs.json')
    logger.debug('Environmental variable CONFIG_PATH set to %s. config_path set to: %s',
                 os.environ.get('CONFIG_PATH'), config_path)
    try:
        with open(config_path, 'r') as f:
            configs = json.load(f)
    except Exception as e:
        logger.error("Can't read or find the specified config file")
        logger.exception(e)
        sys.exit()
    # Sleep to let the elog container start and be ready
    time.sleep(configs.get('initial_sleep', 60))
    while True:
        try:
            logger.info('Parsed configs object: %s', configs)
            # Create a ElogEmailHandler object and from the configs create the appropriate
            # filters to retrieve the emails from the server
            handler = ElogEmailHandler(
                configs['email_hostname'], configs['email_port'], configs['email_user'], configs['email_password'])
            today = pd.to_datetime(
                'now') - pd.Timedelta(configs['runner_time_delta'])
            formatted_date = today.strftime('%d-%b-%Y')
            criteria = ['UNSEEN', f'(SINCE "{formatted_date}")']
            logging.info(
                'The criteria applied to retrieve email is %s', criteria)
            # Retrieve emails as a list of dicts
            emails = handler.get_emails(criteria=criteria)
            logger.info('Retrieved %d emails', len(emails))
            # Transform everything as a dataframe (not necessary but easier to manipulate)
            df = pd.DataFrame(emails)
            # The text content of the email is MIME encoded (RFC 1521), so it must be decoded
            # to bytes and then to unicode string
            df['text'] = df.text.apply(quopri.decodestring).apply(
                lambda x: x.decode('utf8'))
            # indexes to discard from analysis because of error in parsing
            ixs_to_discard = []
            for ix, row in df.iterrows():
                try:
                    # for each email retrieved apply the information extraction function
                    if 'Report' in row.subject:
                        ixs_to_discard.append(ix)
                    parsed_email = handler.parse_email(
                        row['from'], row.subject, row.text)  # returns a dict
                    parsed_email_serie = pd.Series(parsed_email)
                    # add the series as new columns into the main dataframe
                    df.loc[ix, parsed_email_serie.index] = parsed_email_serie.values
                except Exception as e:
                    # if it fails log and save the index that will be dropped later
                    logging.error('Cannot parse email %s, %s, %s',
                                  row['from'], row.subject, row.text)
                    ixs_to_discard.append(ix)
                    logging.error(e, exc_info=True)
                # Drop all the discarded indexes
            logging.debug('Skipping indexes: %s', ixs_to_discard)
            df = df.drop(ixs_to_discard)
            logger.info('Parsed emails content')
            logbook = elog.open(configs['elog_hostname'] + configs['elog_alarm_section'],
                                user=configs['elog_user'], password=configs['elog_password'], use_ssl=configs['elog_use_ssl'])
            # For each email in the dataframe try to search its id in the elog.
            # If not present insert it the elog with the appropriate attributes
            for ix, row in df.iterrows():
                try:
                    entry_id = logbook.search({'Email ID': row.id})
                except Exception as e:
                    logger.error("Trying to search email id %d", row.id)
                    logger.exception(e)
                    continue
                # Check if the parsed email gone wrong. Skip inserting email in elog if that happens
                if (row[['experiment', 'system', 'alarm_name', 'alarm_timestamp']] == 'none').all():
                    continue
                logger.info(
                    'Searching email id %d in logbook. Found elog entry id %s', row.id, entry_id)
                logger.info('Row and dtypes: %s, %s', row, row.dtypes)
                value = round(row.value, 2) if isinstance(
                    row.value, (int, float)) else row.value
                attributes = {
                    'Email ID': row.id,
                    'Email timestamp': row.date.tz_convert('CET').strftime('%Y-%m-%d %H:%M:%S'),
                    'Experiment': row.experiment,
                    'System': row.system,
                    'Alarm timestamp': row.alarm_timestamp.strftime('%Y-%m-%d %H:%M:%S'),
                    'Alarm name': row.alarm_name,
                    'Alarm status': row.alarm_status,
                    'Alarm value': value,
                    'Alarm text': row.parsed_text,
                    'Category': row.category
                }
                # Case 1: no entry to be inserted as already existing and 'elog_override_entry' is false
                if len(entry_id) and not configs.get('elog_override_entry'):
                    logger.debug('Skipping as already present with elog')
                    if configs['elog_override_entry']:
                        logbook.post(
                            row.text, msg_id=entry_id[0], attributes=attributes)
                # Case 2: existing entry to be updated if 'elog_override_entry' is true
                elif len(entry_id) and configs.get('elog_override_entry'):
                    msg_id = entry_id[0]
                    try:
                        edited_msg_id = logbook.post(
                            row.text, msg_id=msg_id, attributes=attributes)
                        logger.debug(
                            f'Updated entry {msg_id} with attributes {attributes}')
                    except elog.LogbookError as e:
                        logger.error(
                            'Error while trying to updating existing id %d', msg_id)
                # Case 3: new entry to be inserted
                else:
                    logger.debug('Email attributes to insert: %s', attributes)
                    try:
                        inserted_id = logbook.post(
                            row.text, author=configs['elog_author'], attributes=attributes, attributes_as_param='value')
                        logger.info(
                            'Inserted new entry with ID: %d', inserted_id)
                        logger.debug('Marking email id %d as seen', row.id)
                        if configs['runner_mark_read_messages']:
                            handler.mark_email(row.id)
                    except elog.LogbookError as e:
                        logger.error(
                            'Error while inserting email id %d', row.id)
                        logger.exception(e)

            # Once the alarm entries are inserted proceed to insert or update
            # the alarm statistics
            stats = ElogAlarmsStats(df)
            # the parsed df is the final result of the parsing and aggregation
            # of the email df
            parsed = stats.get_df_stats()
            logbook = elog.open(configs['elog_hostname'] + configs['elog_alarm_stats_section'],
                                user=configs['elog_user'], password=configs['elog_password'], use_ssl=configs['elog_use_ssl'])
            # For each row in parsed search through the elog to see if the entry exists
            logger.debug('Parsed alarm statistics:')
            logger.debug(parsed)
            for ix, row in parsed.iterrows():
                text = row.subjects
                attributes = row.to_dict()
                del attributes['subjects']  # not interesting for search
                keys = ['experiment', 'year', 'week']
                dict_search = dict(zip(keys, row.loc[keys]))
                logger.debug(
                    'Searching in alarms statistics the following keys %s', dict_search)
                returned_id = logbook.search(
                    dict_search, n_results=configs.get('n_results_search', 300))
                logger.debug('Found id %s for keys %s',
                             returned_id, dict_search)
                if not returned_id:
                    entry_id = logbook.post(
                        text, author=configs['elog_author'], attributes=attributes)
                    logger.info(
                        'Posted new entry id %s, with text %s and attributes %s', entry_id, text, attributes)
                else:
                    entry_id = returned_id[0]
                    if len(returned_id) > 1:
                        ids_to_delete = returned_id[1:]
                        logger.info('Found %d entries with attributes %s. Deleting everything except entry id %d', len(
                            returned_id), attributes, entry_id)
                        for entry in ids_to_delete:
                            logbook.delete(entry)
                    # Update the post just to be sure
                    entry_id = logbook.post(
                        text, msg_id=entry_id, attributes=attributes)
                    logger.info(
                        'Update entry %d with new parsed attributes: %s', entry_id, attributes)

        except Exception as e:
            logger.exception(e)
        time.sleep(configs['runner_time_sleep'])


def test():
    # Setup the logger with the time, logging level, line number and message
    # The logging level can be also changed using the environmental variable
    # 'LOGGING_LEVEL', otherwise it will default to logging.DEBUG (=10)
    logger = logging.getLogger(__name__)
    logging_level = int(os.environ.get('LOGGING_LEVEL', logging.INFO))
    logging.basicConfig(
        level=logging_level, format='%(asctime)s - %(name)s - %(levelname)s - L%(lineno)s - %(message)s')
    # Try to open the json configs to run the program. If no environmental variable
    # is define it will try to open the 'configs.json' file
    config_path = os.environ.get('CONFIG_PATH', 'configs.json')
    logger.debug('Detected config_path: %s', config_path)
    try:
        with open(config_path, 'r') as f:
            configs = json.load(f)
    except Exception as e:
        logger.error("Can't read or find the specified config file")
        logger.exception(e)
        sys.exit()

    logger.info('Parsed configs object: %s', configs)
    # Create a ElogEmailHandler object and from the configs create the appropriate
    # filters to retrieve the emails from the server
    handler = ElogEmailHandler(
        configs['email_hostname'], configs['email_port'], configs['email_user'], configs['email_password'])
    today = pd.to_datetime('now') - pd.Timedelta(configs['runner_time_delta'])
    formatted_date = today.strftime('%d-%b-%Y')
    criteria = ['UNSEEN', f'(SINCE "{formatted_date}")']
    logging.info('The criteria applied to retrieve email is %s', criteria)
    # Retrieve emails as a list of dicts
    emails = handler.get_emails(criteria=criteria)
    logger.info('Retrieved %d emails', len(emails))
    # Transform everything as a dataframe (not necessary but easier to manipulate)
    df = pd.DataFrame(emails)
    # The text content of the email is MIME encoded (RFC 1521), so it must be decoded
    # to bytes and then to unicode string
    df['text'] = df.text.apply(quopri.decodestring).apply(
        lambda x: x.decode('utf8'))
    # for each email retrieved apply the information extraction function
    parsed = pd.DataFrame()

    # indexes to discard from analysis because of error in parsing
    ixs_to_discard = []
    for ix, row in df.iterrows():
        try:
            parsed_email = handler.parse_email(
                row['from'], row.subject, row.text)  # returns a dict
            parsed_email_serie = pd.Series(parsed_email)
            # add the series as new columns into the main dataframe
            df.loc[ix, parsed_email_serie.index] = parsed_email_serie.values
        except Exception as e:
            # if it fails log and save the index that will be dropped later
            logging.error('Cannot parse email %s, %s, %s',
                          row['from'], row.subject, row.text)
            ixs_to_discard.append(ix)
            logging.error(e, exc_info=True)
        # Drop all the discarded indexes
    logging.debug('Skipping indexes: %d', ixs_to_discard)
    df = df.drop(ixs_to_discard)
    logger.info('Parsed emails content')

    # Concat the email dataframe with the parsed dataframe.
    # Each row contains the raw email + parsed email information
    stats = ElogAlarmsStats(df)
    parsed = stats.get_df_stats()

    # Concat the email dataframe with the parsed dataframe.
    # Each row contains the raw email + parsed email information
    return df, stats, parsed

    logbook = elog.open(configs['elog_hostname'] + configs['elog_alarm_stats_section'],
                        user=configs['elog_user'], password=configs['elog_password'], use_ssl=configs['elog_use_ssl'])
    # For each row in parsed search through the elog to see if the entry exists
    logger.debug('Parsed alarm statistics:')
    logger.debug(parsed)
    for ix, row in parsed.iterrows():
        text = row.subjects
        attributes = row.to_dict()
        del attributes['subjects']
        keys = ['experiment', 'week']
        dict_search = dict(zip(keys, row.loc[keys]))
        logger.debug(
            'Searching in alarms statistics the following keys %s', dict_search)
        returned_id = logbook.search(dict_search)
        logger.debug('Found id %s for keys %s', returned_id, dict_search)
        if not returned_id:
            entry_id = logbook.post(text, attributes=attributes)
            logger.info(
                'Posted new entry id %d, with text %s and attributes %s', entry_id, text, attributes)
        else:
            entry_id = returned_id[0]
            # Update the post just to be sure
            entry_id = logbook.post(
                text, msg_id=entry_id, attributes=attributes)
            logger.info(
                'Update entry %d with new parsed attributes: %s', entry_id, attributes)

    return parsed


if __name__ == "__main__":
    main()
